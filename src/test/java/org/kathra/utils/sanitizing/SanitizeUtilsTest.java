/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.utils.sanitizing;

import org.junit.Assert;

import java.util.ArrayList;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */
public class SanitizeUtilsTest {

    @org.junit.Test
    public void sanitizePathParameter() {

        ArrayList<String> paths = new ArrayList();
        paths.add("/test/toto/tata");
        paths.add("/test/toto/tata/");
        paths.add("test/toto/tata/");
        paths.add("test/toto/tata");

        for (String path : paths) {
            Assert.assertEquals("Testing sanitizing", "test/toto/tata", SanitizeUtils.sanitizePathParameter(path));
        }
    }
}