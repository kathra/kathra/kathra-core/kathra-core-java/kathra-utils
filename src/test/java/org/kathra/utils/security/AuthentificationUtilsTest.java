/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */
package org.kathra.utils.security;

import org.junit.Assert;
import org.junit.Test;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;

/**
 * @author julien.boubechtoula
 */
public class AuthentificationUtilsTest {

    @Test
    public void given_private_key_when_formatPrivateKey_then_return_expected_string() throws NoSuchAlgorithmException {
        KeyPair keyPair = AuthentificationUtils.generateRSAKey();
        String privateKeyAsString = AuthentificationUtils.formatPrivateKey(keyPair.getPrivate());
        Assert.assertEquals("-----BEGIN RSA PRIVATE KEY-----\n" +
                java.util.Base64.getMimeEncoder().encodeToString(keyPair.getPrivate().getEncoded()) +
                "\n-----END RSA PRIVATE KEY-----\n",privateKeyAsString);
    }
}
