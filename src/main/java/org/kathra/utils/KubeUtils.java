/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.utils;

import org.yaml.snakeyaml.Yaml;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */
public class KubeUtils {
    public static String defaultAbsolutePathToKubeConfig = System.getenv("HOME")+"/.kube/config";
    private static Yaml y = new Yaml();

    public static void setupIdToken(String absolutePathToKubeConfig) throws FileNotFoundException {
        if(absolutePathToKubeConfig == null || absolutePathToKubeConfig.isEmpty()) {
            absolutePathToKubeConfig=defaultAbsolutePathToKubeConfig;
        }
        Map<String,Object> conf = (Map<String, Object>) y.load(new BufferedInputStream(new FileInputStream(absolutePathToKubeConfig)));
        String currentContext = (String) conf.get("current-context");
        ArrayList<Map> contexts = (ArrayList<Map>) conf.get("contexts");
        String userName = null;
        for (Map context : contexts) {
            if(context.get("name").equals(currentContext)) {
                userName=(String) ((Map)context.get("context")).get("user");
            }
        }

        for (Map user : (ArrayList<Map>) conf.get("users")) {
            if(user.get("name").equals(userName)) {
                String token = (String) ((Map) ((Map) ((Map) user.get("user")).get("auth-provider")).get("config")).get("id-token");
                System.setProperty("kubernetes.auth.token",token);
            }
        }

    }

    public static void main(String[] args) {
        try {
            setupIdToken(null);
            System.out.println(System.getProperty("kubernetes.auth.token"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
