/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.utils.security;

import org.kathra.utils.ConfigManager;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */
public class KeycloakConfig extends ConfigManager {

    private String keycloakAuthUrl;
    private String keycloakKathraAuthUrl;
    private String keycloakRealm;

    private String keycloakClientId;
    private String keycloakClientSecret;

    public KeycloakConfig() {
        keycloakAuthUrl = getProperty("KEYCLOAK_AUTH_URL", "https://keycloak.dev-irtsysx.fr/auth");
        keycloakKathraAuthUrl = getProperty("KEYCLOAK_KATHRA_AUTH_URL", "https://keycloak.dev-irtsysx.fr/auth/realms/kathra/protocol/openid-connect/auth");
        keycloakRealm = getProperty("KEYCLOAK_REALM", "kathra");
        keycloakClientId = getProperty("KEYCLOAK_CLIENT_ID");
        keycloakClientSecret = getProperty("KEYCLOAK_CLIENT_SECRET");
    }

    public String getKeycloakAuthUrl() {
        return keycloakAuthUrl;
    }

    public String getKeycloakKathraAuthUrl() {
        return keycloakKathraAuthUrl;
    }

    public String getKeycloakRealm() {
        return keycloakRealm;
    }

    public String getKeycloakClientId() {
        return keycloakClientId;
    }

    public String getKeycloakClientSecret() {
        return keycloakClientSecret;
    }
}
