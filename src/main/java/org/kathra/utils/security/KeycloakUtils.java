/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.utils.security;

import org.kathra.core.model.Assignation;
import org.kathra.core.model.User;
import org.kathra.utils.Session;
import org.kathra.utils.KathraApiResponse;
import org.kathra.utils.KathraException;
import org.kathra.utils.serialization.GsonUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.keycloak.authorization.client.AuthorizationDeniedException;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.authorization.client.Configuration;
import org.keycloak.authorization.client.representation.TokenIntrospectionResponse;
import org.keycloak.authorization.client.resource.ProtectedResource;
import org.keycloak.authorization.client.util.HttpResponseException;
import org.keycloak.representations.idm.authorization.AuthorizationRequest;
import org.keycloak.representations.idm.authorization.Permission;
import org.keycloak.representations.idm.authorization.ResourceRepresentation;
import org.keycloak.representations.idm.authorization.ScopeRepresentation;
import org.slf4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by qsemanne on 28/09/17.
 */
public class KeycloakUtils {

    static KeycloakConfig config;
    static AuthzClient authzClient;
    static ProtectedResource resourceClient;

    static {
        config = new KeycloakConfig();
        String hostUrl = config.getKeycloakAuthUrl();
        String realm = config.getKeycloakRealm();
        String client = config.getKeycloakClientId();
        String secret = config.getKeycloakClientSecret();

        HashMap<String, Object> sec = new HashMap();
        sec.put("secret", secret);

        Configuration configuration = new Configuration(hostUrl, realm, client, sec, null);
        authzClient = AuthzClient.create(configuration);
        resourceClient = authzClient.protection().resource();
    }

    public static String login(String user, String password) {
        return authzClient.obtainAccessToken(user, password).getToken();
    }

    public static void init() {
        /* Method used to initialize static object */
    }

    public static Map getUserInfos(String accessToken) throws Exception {
        String auth = authzClient.getConfiguration().getAuthServerUrl()
                + "/realms/" + authzClient.getConfiguration().getRealm()
                + "/protocol/openid-connect/userinfo";
        URLConnection connection = new URL(auth).openConnection();
        connection.setRequestProperty("Authorization", "Bearer " + accessToken);
        return GsonUtils.gson.fromJson(IOUtils.toString(connection.getInputStream(), "UTF-8"), HashMap.class);
    }

    public static void handleAuthentication(Session session, Logger logger, String authorization) throws Exception {
        String accessToken;
        KathraException unauthorizedException = new KathraException("Unauthorized. It appears that you don't have permission to access this page. Please make sure you're authorized to view this content.")
                .errorCode(KathraException.ErrorCode.UNAUTHORIZED);
        if (!authorization.isEmpty()) {
            accessToken = authorization.substring(7);
            if (accessToken != null && !accessToken.isEmpty()) {
                try {
                    Map userInfos = getUserInfos(accessToken);
                    User user = new User().firstName((String) userInfos.get("given_name"))
                            .lastName((String) userInfos.get("family_name"))
                            .id((String) userInfos.get("sub"))
                            .email((String) userInfos.get("email"))
                            .name((String) userInfos.get("preferred_username"));
                    session.userObject(user).callerName(user.getName());

                    ((List<String>) userInfos.get("groups"))
                            .stream()
                            .filter(groupPath -> groupPath.startsWith("/kathra-projects"))
                            .map(groupPath -> new Assignation().id(groupPath))
                            .forEach(user::addGroupsItem);

                    if(user.getGroups()==null) user.groups(new ArrayList());
                } catch (IOException e) {
                    e.printStackTrace();
                    if (e.getMessage().contains("401")) {
                        session.setCallerName("Unauthorized user");
                        logger.info("{} - {} \"{}\" {}", session.getCallerAddress(), session.getCallerName(), session.getRequestedOperation(), session.getUserAgent());
                        throw unauthorizedException;
                    } else {
                        throw new KathraException(e.getMessage()).errorCode(KathraException.ErrorCode.INTERNAL_SERVER_ERROR);
                    }
                }
                session.accessToken(accessToken);
                session.authenticated(true);
            }
        }
        if (!session.isAuthenticated()) {
            session.setCallerName("Unauthenticated user");
            logger.info("{} - {} \"{}\" {}", session.getCallerAddress(), session.getCallerName(), session.getRequestedOperation(), session.getUserAgent());
            throw unauthorizedException;
        }

    }

    /**
     * Creates a new resource .
     *
     * @param session            The client session to use
     * @param type               The resource type (e.g.: kathra:resource:user)
     * @param id                 The resource identifier
     * @param scopes             A list of scopes to apply to this resource (e.g.: kathra:scope:user:delete)
     * @param defaultSharedGroup A group name for wich the resource view access will be granted. e.g.: The default/main user's project group
     */
    public static String createResource(Session session, String type, String id, List<String> scopes, String defaultSharedGroup) throws Exception {
        Set<ScopeRepresentation> s_scopes = new HashSet();
        String simpleType = getSimpleTypeFromType(type);

        for (String _scope : scopes) {
            s_scopes.add(new ScopeRepresentation(_scope));
        }
        String name = id;
        String uri = "/" + simpleType + "/" + id;

        return createResourceAllParams(session, type, id, s_scopes, Map.of("owner-group", defaultSharedGroup), name, uri);
    }

    /**
     * Creates a new resource .
     *
     * @param session            The client session to use
     * @param type               The resource type (e.g.: kathra:resource:user)
     * @param id                 The resource identifier
     * @param scopes             A list of scopes to apply to this resource (e.g.: kathra:scope:user:delete)
     * @param metadata           Extra metadata
     */
    public static String createResource(Session session, String type, String id, List<String> scopes, Map<String,String> metadata) throws Exception {
        Set<ScopeRepresentation> s_scopes = new HashSet();
        String simpleType = getSimpleTypeFromType(type);

        for (String _scope : scopes) {
            s_scopes.add(new ScopeRepresentation(_scope));
        }
        String name = id;
        String uri = "/" + simpleType + "/" + id;

        return createResourceAllParams(session, type, id, s_scopes, metadata, name, uri);
    }

    private static String getSimpleTypeFromType(String type) {
        String[] splitType = type.split(":");
        return splitType[splitType.length - 1];
    }

    private static String createResourceAllParams(Session session, String type, String id, Set<ScopeRepresentation> scopes, Map<String,String> metadata, String name, String uri) throws Exception {

        ResourceRepresentation existingResource = resourceClient.findByName(name);
        if (existingResource != null) {
            throw new KathraException("Protected resource " + name + " already exists");
        }

        ResourceRepresentation newResource = new ResourceRepresentation();
        newResource.setType(type);
        newResource.setName(name);
        newResource.setOwner(session.getCallerName());
        newResource.setId(id);
        newResource.setUris(Set.of(uri));
        newResource.setScopes(scopes);

        Map<String, List<String>> customAttributes = new HashMap<>();
        if (metadata != null) {
            for (Map.Entry<String, String> entry : metadata.entrySet()) {
                customAttributes.put(entry.getKey(), Arrays.asList(entry.getValue()));
            }
        }
        newResource.setAttributes(customAttributes);

        return resourceClient.create(newResource).getName();
    }

    /**
     * Gets all resources with the given type.
     *
     * @param session   The client session to use
     * @param type      The resource type (e.g.: kathra:resource:user)
     * @param readScope The "view" scope associated with this resource type (e.g.: kathra:scope:user:view)
     */
    public static List<String> getResourcesByType(Session session, String type, String readScope) throws Exception {
        int maxResult = 500;
        List<String> existingResources = new ArrayList<>();
        List<String> resources;
        do {
            resources = Arrays.asList(resourceClient.find(null, null, null, null, type, StringUtils.isEmpty(readScope) ? null : readScope, false, existingResources.size(), maxResult));
            existingResources.addAll(resources);
        } while(resources.size() == maxResult);
        return  getPermissionsForResources(session, existingResources, List.of(readScope))
                    .parallelStream()
                    .map(permission -> permission.getResourceName())
                    .collect(Collectors.toList());
    }

    /**
     * Gets the unique resource with the given name.
     *
     * @param session   The client session to use
     * @param name      The resource name (e.g.: user-998e8b92-7c85-4e52-895e-c06c12e37b01)
     * @param readScope The "view" scope associated with this resource (e.g.: kathra:scope:user:view)
     */
    public static String getResourceByName(Session session, String name, String readScope) throws Exception {

        List<String> resources = new LinkedList<>();
        ResourceRepresentation res = resourceClient.findByName(name);

        if (res != null) {
            resources.add(res.getId());
            return getPermissionsForResources(session, resources, Collections.singletonList(readScope)).get(0).getResourceName();
        } else throw new KathraException("Requesteds resource " + name + " does not exist");
    }

    /**
     * Gets the unique resource with the given id.
     *
     * @param session   The client session to use
     * @param id        The resource id (e.g.: 998e8b92-7c85-4e52-895e-c06c12e37b01)
     * @param readScope The "view" scope associated with this resource (e.g.: kathra:scope:user:view)
     */
    public static String getResourceById(Session session, String id, String readScope) throws Exception {

        List<String> resources = new LinkedList<>();
        ResourceRepresentation res = null;
        try {
            res = resourceClient.findById(id);
        } catch(RuntimeException e) {
            if (e.getCause()!=null && HttpResponseException.class.isInstance(e.getCause())
                    && ((HttpResponseException)e.getCause()).getStatusCode() == KathraApiResponse.HttpStatusCode.NOT_FOUND.getCode()) {
                throw new KathraException("Resource not found : "+id,e,KathraException.ErrorCode.NOT_FOUND);
            }
        }
        if (res != null) {
            resources.add(res.getId());
            List<Permission> permissions = getPermissionsForResources(session, resources, Collections.singletonList(readScope));
            return permissions.isEmpty() ? null : permissions.get(0).getResourceName();
        } else throw new KathraException("Internal error while requesting resource : " + id);
    }

    private static List<Permission> getPermissionsForResources(Session session, List<String> resources, List<String> scopes) throws Exception {

        String eat = session.getAccessToken();
        AuthorizationRequest req = new AuthorizationRequest();
        for (String resourceId : resources) {
            req.addPermission(resourceId, scopes);
        }

        TokenIntrospectionResponse rpt = null;
        
        try {
            String strRpt = authzClient.authorization(eat).authorize(req).getToken();
            rpt = authzClient.protection().introspectRequestingPartyToken(strRpt);
        } catch (AuthorizationDeniedException e) {
            e.printStackTrace();
            return new ArrayList();
        }

        if (rpt != null && rpt.getActive()) {
            return rpt.getPermissions();
        } else throw new KathraException("RPT is inactive").errorCode(KathraException.ErrorCode.INTERNAL_SERVER_ERROR);
    }

    /**
     * Deletes the unique resource with the given id.
     *
     * @param session     The client session to use
     * @param id          The resource id (e.g.: user-998e8b92-7c85-4e52-895e-c06c12e37b01)
     * @param deleteScope The "delete" scope associated with this resource (e.g.: kathra:scope:user:delete)
     */
    public static void deleteResource(Session session, String id, String deleteScope) throws Exception {

        List<String> resources = new LinkedList<>();
        ResourceRepresentation res = resourceClient.findById(id);

        if (res != null) {
            resources.add(res.getId());
            String resourceId = getPermissionsForResources(session, resources, Collections.singletonList(deleteScope)).get(0).getResourceId();
            authzClient.protection().resource().delete(resourceId);
        } else throw new KathraException("Requesteds resource with id " + id + " does not exist");
    }

    /**
     * Deletes the unique resource with the given id.
     *
     * @param session     The client session to use
     * @param id          The resource id (e.g.: user-998e8b92-7c85-4e52-895e-c06c12e37b01)
     * @param scopeToDelete The "delete" scope associated with this resource (e.g.: kathra:scope:user:delete)
     */
    public static void deleteResourceScope(Session session, String id, String scopeToDelete) throws Exception {

        List<String> resources = new LinkedList<>();
        ResourceRepresentation res = resourceClient.findById(id);

        if (res != null) {
            resources.add(res.getId());
            res.setScopes(res.getScopes().stream().filter(scope -> !scope.getName().equals(scopeToDelete)).collect(Collectors.toSet()));
            authzClient.protection().resource().update(res);
        } else throw new KathraException("Requesteds resource with id " + id + " does not exist");
    }
}