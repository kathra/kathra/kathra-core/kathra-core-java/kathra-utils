/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.utils;

import org.kathra.core.model.User;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */
public class Session {
    private String id;
    private String callerName;
    private String callerAddress;
    private String requestedOperation;
    private String accessToken;
    private String userAgent;
    private User userObject;
    private boolean authenticated;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Session id(String id) {
        this.id = id;
        return this;
    }

    public String getCallerName() {
        return callerName;
    }

    public void setCallerName(String callerName) {
        this.callerName = callerName;
    }

    public Session callerName(String callerName) {
        this.callerName = callerName;
        return this;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public Session authenticated(boolean authenticatedSession) {
        this.authenticated = authenticatedSession;
        return this;
    }

    public Session callerAddress(String callerAddress) {
        this.callerAddress = callerAddress;
        return this;
    }

    public Session requestedOperation(String requestedOperation) {
        this.requestedOperation = requestedOperation;
        return this;
    }

    public String getCallerAddress() {
        return callerAddress;
    }

    public void setCallerAddress(String callerAddress) {
        this.callerAddress = callerAddress;
    }

    public String getRequestedOperation() {
        return requestedOperation;
    }

    public void setRequestedOperation(String requestedOperation) {
        this.requestedOperation = requestedOperation;
    }

    public Session accessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Session userAgent(String userAgent) {
        this.userAgent = userAgent;
        return this;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public User getUserObject() {
        return userObject;
    }

    public Session userObject(User userObject) {
        this.userObject = userObject;
        return this;
    }

    public void setUserObject(User userObject) {
        this.userObject = userObject;
    }
}
