/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


/**
 * @author chi-dung.tran
 */
public class ZipUtils {

    private ZipUtils() {
    }

    public static String getTopLevelFolderNameInZipFile(File currentZipFile) throws IOException {

        try (ZipFile zipFile = new ZipFile(currentZipFile)) {
            List<String> result = new ArrayList<>();
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                if (entry.isDirectory()) {
                    File file = new File(entry.getName());
                    if (file.getParent() == null) {
                        result.add(file.getName());
                    }
                }
            }

            if ((result != null) && (result.size() == 1))
                return result.get(0);
            else throw new IOException("More than one folder in provided zip file");
        }
    }
}