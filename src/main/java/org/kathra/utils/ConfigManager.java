/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.utils;

import java.util.Properties;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */
public abstract class ConfigManager {

    Properties properties = new Properties();

    protected String getProperty(String name) throws KathraRuntimeException {

        String tmp = System.getenv(name);

        if (tmp != null) {
            tmp = tmp.trim();
            if (!tmp.isEmpty()) {
                return tmp;
            }
        }

        tmp = properties.getProperty(name);

        if (tmp != null) {
            tmp = tmp.trim();
            if (!tmp.isEmpty()) {
                return tmp;
            }
        }
        throw new KathraRuntimeException("Configuration property " + name + " is missing, please check environment variables or config file.").errorCode(KathraRuntimeException.ErrorCode.MISSING_CONFIGURATION_PARAMETER);
    }

    protected String getProperty(String name, String defaultValue) {

        String tmp = System.getenv(name);

        if (tmp != null) {
            tmp = tmp.trim();
            if (!tmp.isEmpty()) {
                return tmp;
            }
        }

        tmp = properties.getProperty(name);

        if (tmp != null) {
            tmp = tmp.trim();
            if (!tmp.isEmpty()) {
                return tmp;
            }
        }

        return defaultValue;
    }
}
