/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.utils;

/**
 * @author Jérémy Guillemot <Jeremy.Guillemot@kathra.org>
 */
public class KathraException extends Exception {

    ErrorCode errorCode;

    public KathraException(String message) {
        super(message);
    }

    public KathraException(String message, Throwable cause) {
        super(message, cause);
    }
    public KathraException(String message, Throwable cause, ErrorCode errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode statusCode) {
        this.errorCode = statusCode;
    }

    public KathraException errorCode(ErrorCode errorCode) {
        setErrorCode(errorCode);
        return this;
    }

    // LICENSE GPL 3.0 Peter Pilgrim, Xenonique.co.uk, 2012
    // http://www.gnu.org/licenses/gpl.html GNU PUBLIC LICENSE 3.0


    /**
     * Implementation type HttpErrorCode
     * @see "http://en.wikipedia.org/wiki/List_of_HTTP_status_codes"
     */
    public enum ErrorCode {

        MULTIPLE_CHOICES(300, "Multiple Choices"),
        MOVED_PERMANENTLY(301, "Moved Permanently"),
        FOUND(302, "Found"),
        SEE_OTHER(303, "See Other (since HTTP/1.1)"),
        NOT_MODIFIED(304, "Not Modified"),
        USE_PROXY(305, "Use Proxy (since HTTP/1.1)"),
        SWITCH_PROXY(306, "Switch Proxy"),
        TEMPORARY_REDIRECT(307, "Temporary Redirect (since HTTP/1.1)"),
        PERMANENT_REDIRECT(308, "Permanent Redirect (approved as experimental RFC)[12]"),

        BAD_REQUEST(400, "Bad Request"),
        UNAUTHORIZED(401, "Unauthorized"),
        PAYMENT_REQUIRED(402, "Payment Required"),
        FORBIDDEN(403, "Forbidden"),
        NOT_FOUND(404, "Not Found"),
        METHOD_NOT_ALLOWED(405, "Method Not Allowed"),
        NOT_ACCEPTABLE(406, "Not Acceptable"),
        PROXY_AUTHENTICATION_REQUIRED(407, "Proxy Authentication Required"),
        REQUEST_TIMEOUT(408, "Request Timeout"),
        CONFLICT(409, "Conflict"),
        GONE(410, "Gone"),
        LENGTH_REQUIRED(411, "Length Required"),
        PRECONDITION_FAILED(412, "Precondition Failed"),
        REQUEST_ENTITY_TOO_LARGE(413, "Request Entity Too Large"),
        REQUEST_URI_TOO_LONG(414, "Request-URI Too Long"),
        UNSUPPORTED_MEDIA_TYPE(415, "Unsupported Media Type"),
        REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested Range Not Satisfiable"),
        EXPECTATION_FAILED(417, "Expectation Failed"),

        INTERNAL_SERVER_ERROR(500, "Internal Server Error"),
        NOT_IMPLEMENTED(501, "Not Implemented"),
        BAD_GATEWAY(502, "Bad Gateway"),
        SERVICE_UNAVAILABLE(503, "Service Unavailable"),
        GATEWAY_TIMEOUT(504, "Gateway Timeout"),
        HTTP_VERSION_NOT_SUPPORTED(505, "HTTP Version Not Supported"),
        VARIANT_ALSO_NEGOTIATES(506, "Variant Also Negotiates (RFC 2295)"),
        INSUFFICIENT_STORAGE(507, "Insufficient Storage (WebDAV; RFC 4918)"),
        LOOP_DETECTED(508, "Loop Detected (WebDAV; RFC 5842)"),
        BANDWIDTH_LIMIT_EXCEEDED(509, "Bandwidth Limit Exceeded (Apache bw/limited extension)"),
        NOT_EXTEND(510, "Not Extended (RFC 2774)"),
        NETWORK_AUTHENTICATION_REQUIRED(511, "Network Authentication Required (RFC 6585)"),
        CONNECTION_TIMED_OUT(522, "Connection timed out"),
        PROXY_DECLINED_REQUEST(523, "Proxy Declined Request"),
        TIMEOUT_OCCURRED(524, "A timeout occurred"),

        MISSING_CONFIGURATION_PARAMETER(600,"Missing parameter in configuration file");

        private int code;
        private String desc;
        private String text;

        ErrorCode(int code, String desc) {
            this.code = code;
            this.desc = desc;
            this.text = Integer.toString(code);
        }

        /**
         * Gets the HTTP status code
         * @return the status code number
         */
        public int getCode() {
            return code;
        }

        /**
         * Gets the HTTP status code as a text string
         * @return the status code as a text string
         */
        public String asText() {
            return text;
        }

        /**
         * Get the description
         * @return the description of the status code
         */
        public String getDesc() {
            return desc;
        }

    }
}
