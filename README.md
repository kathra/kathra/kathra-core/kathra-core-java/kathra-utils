# Kathra-utils

## Keycloak utils

To use the functions provided by this utility, you need to definine the following runtime parameters to the JVM :

- KEYCLOAK_AUTH_URL : keycloak server authorization endpoint (default : "http://keycloak.acl.irtsysx.fr/auth");
- KEYCLOAK_REALM : keycloak realm (default :"KATHRA");
- KEYCLOAK_CLIENT_NAME : keycloak client name (Mandatory)
- KEYCLOAK_CLIENT_SECRET : keycloak client secret (Mandatory)